import axios from 'axios';

class WalletService {
    constructor() {
        this.httpClient = axios.create({
            baseURL: "http://localhost:8001/api/v1",
            timeout: 30000,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        });
    }

    getCredit(user_id) {
        return new Promise((ok, reject) => {
            this.httpClient.get(`/users/${user_id}/credit`)
                .then(r => {
                    let credit = r.data.data.credit;

                    ok(credit);
                }).catch(e => {
                    if (e.response && e.response.data && e.response.data.data) {
                        reject(e.response.data.data.error);
                    } else {
                        reject(e);
                    }
                });
        });
    }

    applyCoupon(user_id, coupon_code) {
        return new Promise((ok, reject) => {
            this.httpClient.post(`/coupons/apply`, {
                coupon_code: coupon_code,
                user_id: user_id,
            })
                .then(r => {
                    ok(r.data.data.msg);
                }).catch(e => {
                    if (e.response && e.response.data && e.response.data.data) {
                        reject(e.response.data.data.error);
                    } else {
                        reject(e);
                    }
                });
        });
    }

    fetchTransactions(user_id, page = 0, limit = 10) {
        return new Promise((ok, reject) => {
            this.httpClient.get(`/users/${user_id}/transactions`, {
                params: {
                    limit: limit,
                    page: page,
                }
            })
                .then(r => {
                    let transactions = r.data.data.transactions;

                    ok(transactions);
                }).catch(e => {
                    if (e.response && e.response.data && e.response.data.data) {
                        reject(e.response.data.data.error);
                    } else {
                        reject(e);
                    }
                });
        });
    }

}

export default new WalletService();