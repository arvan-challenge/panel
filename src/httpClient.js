import axios from 'axios';

class HttpClient {
    constructor(baseURL, timeout=3000) {
        if (instance) {
            return instance;
        }

        instance = axios.create({
            baseURL: baseURL,
            timeout: timeout,
            headers: { 'X-Custom-Header': 'foobar' }
        });
    }
}

export default HttpClient;