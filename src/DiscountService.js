import axios from 'axios';

class DiscountService {
    constructor() {
        this.httpClient = axios.create({
            baseURL: "http://localhost:8000/api/v1",
            timeout: 30000,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        });
    }

    fetchCoupons(page = 0, limit = 10) {
        return new Promise((ok, reject) => {
            this.httpClient.get(`/coupons`, {
                params: {
                    limit: limit,
                    page: page,
                }
            })
                .then(r => {
                    let coupons = r.data.data.coupons;

                    ok(coupons);
                }).catch(e => {
                    if (e.response && e.response.data && e.response.data.data) {
                        reject(e.response.data.data.error);
                    } else {
                        reject(e);
                    }
                });
        });
    }

    fetchCouponUsages(coupon_code, page = 0, limit = 10) {
        return new Promise((ok, reject) => {
            this.httpClient.get(`/coupons/${coupon_code}/usages`, {
                params: {
                    limit: limit,
                    page: page,
                }
            })
                .then(r => {
                    let usages = r.data.data.usages;

                    ok(usages);
                }).catch(e => {
                    if (e.response && e.response.data && e.response.data.data) {
                        reject(e.response.data.data.error);
                    } else {
                        reject(e);
                    }
                });
        });
    }

}

export default new DiscountService();